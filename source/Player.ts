import {Game, GameEvent} from './Game';

export default class Player {
    private readonly _name: string;

    private _isReady: boolean = false;
    private _readyReset: NodeJS.Timeout;
    static readonly READY_TIMEOUT = 5000;

    onGameEvent: (event: GameEvent, info: any) => void;
    onStartGame: () => void;

    game: Game;
    wins = 0;
    losses = 0;

    constructor(name: string) {
        this._name = name;
    }

    startGame() {
        this.isReady = false;
        this.onStartGame();
    }

    set isReady(value: boolean) {
        if (this._readyReset) clearTimeout(this._readyReset);
        this._isReady = value;

        if (value) {
            this._readyReset = setTimeout(() => this._isReady = false, Player.READY_TIMEOUT);
        }
    }

    get isReady(): boolean {
        return this._isReady;
    }

    get name(): string {
        return this._name;
    }
}
