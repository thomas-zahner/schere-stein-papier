import Player from './Player';

export interface Score {
    name: string,
    wins: number,
    losses: number
}

export class PlayerList {
    players: Player[] = [];

    playerExists(playerName: string) {
        return this.players.some(player => player.name === playerName);
    };

    addPlayer(playerName: string) {
        if (!playerName) throw new Error(`Invalid value '${playerName}' as player name`);
        if (this.playerExists(playerName)) throw new Error(`Player with name '${playerName}' already exits`);

        this.players.push(new Player(playerName));
    }

    getPlayer(playerName: string) {
        return this.players.find(player => player.name === playerName);
    }

    getScoreboard(): Score[] {
        const scores: Score[] = [];
        this.players.forEach(player => scores.push({name: player.name, wins: player.wins, losses: player.losses}));
        scores.sort((a, b) => b.wins - a.wins);
        return scores;
    }
}
