import Player from './Player';

export enum Move {
    rock = 1, paper, scissors, spock, lizard
}

export enum GameEvent {
    roundPreparing, roundStart, gameEnded
}

const moveToString = (move: Move) => {
    switch (move) {
        case Move.rock:
            return 'Stein';
        case Move.paper:
            return 'Papier';
        case Move.scissors:
            return 'Schere';
        case Move.spock:
            return 'Spock';
        case Move.lizard:
            return 'Echse';
    }
};

enum RoundState {
    preparation, active, finished
}

class Round {
    private _player1Move: Move;
    private _player2Move: Move;

    private readonly player1: Player;
    private readonly player2: Player;

    private _state = RoundState.preparation;

    private setFinished: NodeJS.Timeout;
    private setActive: NodeJS.Timeout;

    static PREPARE_TIME_MS = 5000;
    static ACTIVE_TIME_MS = 7000;

    constructor(player1: Player, player2: Player, onRoundEnd: () => void, onRoundStart: () => void) {
        this.player1 = player1;
        this.player2 = player2;

        const setFinished = () => {
            this._state = RoundState.finished;
            onRoundEnd();
        };

        const setActive = () => {
            this._state = RoundState.active;
            this.setFinished = setTimeout(() => setFinished(), Round.ACTIVE_TIME_MS);
            onRoundStart();
        };

        this.setActive = setTimeout(() => setActive(), Round.PREPARE_TIME_MS);
    }

    abort() {
        if (this.setActive) clearTimeout(this.setActive);
        if (this.setFinished) clearTimeout(this.setFinished);
        this._state = RoundState.finished;
    }

    getWinner(): Player | 'draw' {
        if (this._state !== RoundState.finished) {
            throw new Error('Cannot get winner because round isn\'t finished');
        }

        if (!this._player1Move || !this._player2Move) {
            return null;
        }

        const result = (5 + this._player1Move - this._player2Move) % 5;

        if (result === 0) return 'draw';
        if (result % 2 === 1) return this.player1;
        return this.player2;
    }

    get state(): RoundState {
        return this._state;
    }

    getMove(player: Player) {
        if (player === this.player1) return this._player1Move;
        if (player === this.player2) return this._player2Move;
    }

    set player1Move(move: Move) {
        if (this.state === RoundState.active) this._player1Move = move;
    }

    set player2Move(move: Move) {
        if (this.state === RoundState.active) this._player2Move = move;
    }
}

export class Game {
    private readonly player1: Player;
    private readonly player2: Player;
    private player1Score = 0;
    private player2Score = 0;
    private rounds: Round[] = [];

    private active = true;

    constructor(player1: Player, player2: Player) {
        this.player1 = player1;
        this.player2 = player2;
        this.newRound();
    }

    makeMove(player: Player, move: Move) {
        if (player === this.player1) this.currentRound().player1Move = move;
        if (player === this.player2) this.currentRound().player2Move = move;
    }

    stopGame(reason: 'quit' | 'inactivity') {
        if (!this.active) return;
        this.active = false;
        this.currentRound().abort();

        this.player1.wins += this.player1Score;
        this.player2.wins += this.player2Score;
        this.player1.losses += this.player2Score;
        this.player2.losses += this.player1Score;

        this.player1.game = null;
        this.player2.game = null;

        this.triggerEvent(GameEvent.gameEnded, reason);
    }

    getScore() {
        return {player1: this.player1Score, player2: this.player2Score};
    }

    getOpponentOf(player: Player) {
        if (this.player1 === player) return this.player2;
        if (this.player2 === player) return this.player1;
    }

    getPlayerNames() {
        return {
            player1: this.player1.name,
            player2: this.player2.name
        };
    }

    getHistory(viewAs?: Player) {
        return this.rounds.filter(round => round.state === RoundState.finished).reverse().map(round => {
            return {
                player1Move: moveToString(round.getMove(this.player1)),
                player2Move: moveToString(round.getMove(this.player2)),
                personalResult: this.getResultFor(round, viewAs)
            };
        })
    }

    getLastRoundResultFor(player: Player) {
        const round = this.getLastFinishedRound();
        const opponent = this.getOpponentOf(player);
        if (!round || !opponent) return null;

        return {
            result: this.getResultFor(round, player),
            opponentMove: moveToString(round.getMove(opponent))
        }
    }

    private getLastFinishedRound() {
        const finishedRounds = this.rounds.filter(round => round.state === RoundState.finished);
        if (finishedRounds.length < 1) return null;
        return finishedRounds[finishedRounds.length - 1];
    }

    private getResultFor(round: Round, player: Player) {
        const winner = round.getWinner();

        if (winner && (player === this.player1 || player === this.player2)) {
            if (winner === 'draw') return 'draw';
            else return winner === player ? 'win' : 'defeat';
        }
    }

    private onRoundEnd() {
        const winner = this.currentRound().getWinner();

        if (winner) {
            if (winner === this.player1) this.player1Score++;
            if (winner === this.player2) this.player2Score++;
            this.newRound();
        } else {
            this.stopGame('inactivity');
        }
    }

    private triggerEvent(event: GameEvent, info: any) {
        if (this.player1.onGameEvent) this.player1.onGameEvent(event, info);
        if (this.player2.onGameEvent) this.player2.onGameEvent(event, info);
    }

    private onRoundStart() {
        this.triggerEvent(GameEvent.roundStart, {until: Round.ACTIVE_TIME_MS});
    }

    private newRound() {
        this.triggerEvent(GameEvent.roundPreparing, {until: Round.PREPARE_TIME_MS});
        this.rounds.push(new Round(this.player1, this.player2, this.onRoundEnd.bind(this), this.onRoundStart.bind(this)));
    }

    private currentRound() {
        return this.rounds[this.rounds.length - 1];
    }
}