import {NextFunction, Request, Response} from 'express';
import {PlayerList} from './PlayerList';
import {Game, GameEvent, Move} from './Game';
import Player from './Player';

const path = require('path');
const express = require('express');
const session = require('express-session');
const {check, validationResult} = require('express-validator');
const crypto = require('crypto');

const port = 8080;
const host = 'localhost';
const content = path.join(__dirname, '../public/');

const playerList = new PlayerList();

const app = express();
app.use(express.json());

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: crypto.randomBytes(32).toString('hex'),
    cookie: {
        httpOnly: true,
        sameSite: true,
        maxAge: 1000 * 60 * 60 * 24
    }
}));

const usernameCheck = check('username', 'Name muss 3-20 Zeichen lang sein').isLength({min: 3, max: 20});

app.get('/', (req: Request, res: Response) => {
    if (!req.session.username) {
        res.sendFile(path.join(content, 'setUsername.html'));
    } else if (!playerList.getPlayer(req.session.username).game) {
        res.sendFile(path.join(content, 'index.html'));
    } else {
        res.sendFile(path.join(content, 'game.html'));
    }
});

app.post('/setUsername', [usernameCheck], (req: Request, res: Response) => {
    validationResult(req).throw();

    const username: string = req.body.username;
    if (playerList.playerExists(username)) {
        return res.send({errorMessage: 'Name schon vergeben'});
    }

    playerList.addPlayer(username);
    req.session.username = username;
    res.send({redirect: '/'});
});

app.get('/pollGame', (req: Request, res: Response) => {
    const player = playerList.getPlayer(req.session.username);
    if (!player || player.game) return res.status(400).end();

    player.onStartGame = () => res.end();
});

app.get('/ready', (req: Request, res: Response) => {
    const player = playerList.getPlayer(req.session.username);
    if (!player || player.game) return res.status(400).end();

    player.isReady = true;
    res.send({timeout: Player.READY_TIMEOUT});
});

app.post('/joinGame', [usernameCheck], (req: Request, res: Response) => {
    validationResult(req).throw();
    if (!req.session.username) {
        return res.status(400).send({redirect: '/'});
    }

    const opponent = playerList.getPlayer(req.body.username);
    const self = playerList.getPlayer(req.session.username);

    if (!opponent) {
        res.send({errorMessage: 'Gegner nicht gefunden'});
    } else if (opponent.name === self.name) {
        res.send({errorMessage: 'Unmöglich sich selbst beizutreten'});
    } else if (!opponent.isReady) {
        res.send({errorMessage: 'Gegner ist nicht bereit'});
    } else {
        const game = new Game(opponent, self);
        opponent.game = game;
        self.game = game;

        opponent.startGame();
        self.startGame();
    }
});

app.post('/move', [check('move').isIn(Move)], (req: Request, res: Response) => {
    validationResult(req).throw();

    const player = playerList.getPlayer(req.session.username);
    if (!player || !player.game) return res.status(400).send({});
    if (!req.body.move) return res.status(400).send({});

    const move: Move = Move[req.body.move as keyof typeof Move];
    if (!move) return res.status(400).send({});

    player.game.makeMove(player, move);
    res.send({});
});

app.get('/pollGameEvent', (req: Request, res: Response) => {
    const player = playerList.getPlayer(req.session.username);
    if (!player || !player.game) return res.status(400).send({});

    player.onGameEvent = (event, info: any) => {
        if (event === GameEvent.roundPreparing) {
            info.previousRound = player.game.getLastRoundResultFor(player);
        }

        res.send({event: GameEvent[event], info: info});
        delete player.onGameEvent;
    };
});

app.get('/gameStats', (req: Request, res: Response) => {
    const player = playerList.getPlayer(req.session.username);
    if (!player || !player.game) return res.status(400).send({});

    res.send({
        score: player.game.getScore(),
        history: player.game.getHistory(player),
        playerNames: player.game.getPlayerNames()
    });
});

app.get('/quitGame', (req: Request, res: Response) => {
    const player = playerList.getPlayer(req.session.username);
    if (!player || !player.game) return res.end();

    player.game.stopGame('quit');
    res.end();
});

app.get('/scoreboard', (req: Request, res: Response) => {
    res.send(playerList.getScoreboard());
});

//Error handler
app.use((err: Error, req: Request, res: Response, _next: NextFunction) => {
    if (!validationResult(req).isEmpty()) {
        const validationErrors: any = {};
        validationResult(req).array().forEach((error: any) => validationErrors[error.param] = error.msg);
        return res.status(400).send({validationErrors: validationErrors});
    }

    console.error(err.stack);
    res.status(500).end();
});

app.use(express.static(content));
app.listen(port, host, () => console.log(`Running on http://${host}:${port}`));
