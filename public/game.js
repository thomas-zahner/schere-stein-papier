let roundActive = false;

function forEachInputButton(action) {
  document.querySelectorAll('#game-moves > input').forEach(input => action(input));
}

function displayInfo(info) {
  document.querySelector('div#info').textContent = info;
}

function displayOpponentMove(move) {
  if (move) document.querySelector('input#opponent-move').value = move;
}

function renderHistory(history, playerNames) {
  const table = document.querySelector('table#history');
  while (table.hasChildNodes()) table.removeChild(table.lastChild);

  history.map(round => round.personalResult = resultToString(round.personalResult));
  addTableHead(table, [playerNames.player1, playerNames.player2, 'Resultat']);
  addTableBody(table, history);
}

function renderScore(score, playerNames) {
  const div = document.querySelector('div#score');
  div.textContent = `${playerNames.player1}: ${score.player1} / ${playerNames.player2}: ${score.player2}`;
}

function updateStats() {
  fetch('/gameStats').then(data => data.json()).then(stats => {
    renderScore(stats.score, stats.playerNames);
    renderHistory(stats.history, stats.playerNames);
  });
}

function resultToString(result) {
  switch (result) {
    case 'draw':
      return 'Unentschieden';
    case 'win':
      return 'Du hast gewonnen';
    case 'defeat':
      return 'Du hast verloren';
  }
}

function onRoundPrepare(info) {
  roundActive = false;
  const previousRound = info.previousRound;

  if (previousRound) {
    displayInfo(resultToString(previousRound.result));
    displayOpponentMove(previousRound.opponentMove);
  }

  setProgressBar('0%', info.until);
  updateStats();
  pollGameEvent();
}

function onRoundStart(info) {
  roundActive = true;
  deselectInputMoves();

  displayInfo('Spiele etwas!');
  displayOpponentMove('?');

  setProgressBar('100%', info.until);
  pollGameEvent();
}

function onGameEnd(info) {
  roundActive = false;
  if (info === 'quit') displayInfo('Der Gegner hat das Spiel verlassen');
  if (info === 'inactivity') displayInfo('Nicht beide Spieler haben etwas gespielt');

  setProgressBar('0%', 1000);
  document.querySelector('#quitGame').value = 'Zurück';

  forEachInputButton(input => {
    input.onclick = null;
    input.style.cursor = 'not-allowed';
  });
}

function pollGameEvent() {
  fetch('/pollGameEvent').then(data => data.json()).then(data => {
    if (data.event === 'roundPreparing') onRoundPrepare(data.info);
    else if (data.event === 'roundStart') onRoundStart(data.info);
    else if (data.event === 'gameEnded') onGameEnd(data.info);
  });
}

function setProgressBar(width, timeMs) {
  const bar = document.querySelector('#bar');
  bar.style.transition = `width ${timeMs}ms`;
  bar.style.width = width;
}

function selectInputMove(input) {
  postRequest('/move', { move: input.id });
  deselectInputMoves();
  input.classList.add('active');
}

function deselectInputMoves() {
  forEachInputButton(input => input.classList.remove('active'));
}

window.onload = () => {
  forEachInputButton(input => input.onclick = () => {
    if (roundActive) {
      selectInputMove(input);
    }
  });

  document.querySelector('#quitGame').onclick = () => {
    fetch('/quitGame').then(_ => window.location.href = '/');
  };

  updateStats();
  pollGameEvent();
};
