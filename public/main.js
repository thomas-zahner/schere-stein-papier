function postRequest(url, data) {
  return fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  }).then(response => {
    if (!response.ok) {
      console.error(`${response.statusText}: ${url}`);
    }

    const json = response.json();

    json.then(data => {
      if (typeof data.redirect === 'string') {
        window.location.href = data.redirect;
      }

      const errorDiv = document.querySelector('div#error');

      if (typeof data.validationErrors === 'object') {
        if (errorDiv) errorDiv.textContent = '';

        Object.keys(data.validationErrors).forEach(key => {
          console.error(key + ': ' + data.validationErrors[key]);
          if (errorDiv) errorDiv.textContent += data.validationErrors[key] + '\n';
        });
      }

      if (errorDiv && typeof data.errorMessage === 'string') {
        errorDiv.textContent = data.errorMessage;
      }
    });

    return json;
  });
}

function addTableHead(table, data) {
  const thead = table.createTHead();
  const row = thead.insertRow();

  for (const element of data) {
    const th = document.createElement('th');
    th.appendChild(document.createTextNode(element));
    row.appendChild(th);
  }
}

function addTableBody(table, data) {
  const tbody = table.createTBody();

  for (const object of data) {
    const row = tbody.insertRow();
    for (const key in object) {
      const cell = row.insertCell();
      cell.appendChild(document.createTextNode(object[key]));
    }
  }
}