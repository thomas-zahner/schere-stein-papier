window.onload = () => {
  document.querySelector('form').onsubmit = () => {
    postRequest('/setUsername', { username: document.querySelector('#username').value });
    return false;
  }
};
