fetch('/pollGame').then(_ => window.location.href = '/');

function renderScoreboard() {
  const scoreboard = document.querySelector('table#scoreboard');
  addTableHead(scoreboard, ['Name', 'Gewonnen', 'Verloren']);
  fetch('/scoreboard').then(data => data.json()).then(data => addTableBody(scoreboard, data));
}

function addSubmitEventListener() {
  document.querySelector('#join').onsubmit = () => {
    postRequest('/joinGame', { username: document.querySelector('#username').value });
    return false;
  };
}

function sendReadyStatus() {
  fetch('/ready').then(data => data.json()).then(data => {
    if (typeof data.timeout === 'number') {
      setTimeout(sendReadyStatus, data.timeout - 1000);
    }
  });
}

window.onload = () => {
  addSubmitEventListener();
  renderScoreboard();
  sendReadyStatus();
};
